## Pleroma

**pleroma** (ple·​ro·​ma | \ plə̇ˈrōmə \): the state of total fullness or abundance

A simple script to get a beautiful and fully contigured BSPWM setup running on [Void Linux](https://voidlinux.org/) along with some useful applications and utilities. Think of it like [LARBS](https://larbs.xyz/), but for BSPWM (thanks, Reddit).

HUGE thanks to [whereismycow42](https://www.reddit.com/user/whereismycow42/) for the improvement suggestions.

<img src="screenshot.png" width=500>

### Step 1: Install Void Linux
Pleroma **does not** actually install [Void Linux](https://voidlinux.org) so please install the distro before running this script. Installation is nearly identical for both Glibc and Musl variants, but if you are using Musl, the NVIDIA proprietary driver installation will be skipped. The minimal ISO install is recommended as Pleroma will install BSPWM and assumes no other window manager or desktop environment is installed.

### Step 2: Clone the repo
```
sudo xbps-install -Sy git
git clone https://gitlab.com/ferrettim/pleroma.git
```

### Step 3: Run the install script
The script itself is pretty easy to read so feel free to edit your package install preferences. You probably only want to change the list of applications listed under "additional applications" in the install script, but if you know what you're doing, edit at your leisure. When you're done editing your package selection...
```
bash plemora/install.sh
```
**NOTE:** If you run the script from another directory, your dotfiles will not be copied over. See the "Importing dotfiles..." section of install.sh on how to fix this.

### Applications used for desktop environment
- BSPWM (Window Manager)
- Polybar (Toolbar)
- sxhkd (Keybindings)
- picom (Compositor)
- uxvrt (Terminal emulator)
- Betterlockscreen (minimal screenlock)
- Bottom (system monitor)
- Ranger (file manager) with ranger_archives plugin and theme matching desktop theme.
- Cava (Audio visualizer)
- Dunst (notification manager)

### Fonts installed (required by dot files)
- Ioveska
- Material Design

### Additional applications installed
- Atom (code editor) **Only installed on Glibc systems**
- Audacity (audio editor)
- ckb-next (LED control for Corsair keyboard/mice)
- Firefox (web browser)
- Flameshot (screenshot utility)
- Flatpak support with Flathub repos
- GIMP (image editing)
- LibreOffice Writer and Calc (along with the Gnome theme and English language packs)
- Lutris (gaming)
- MPV (audio and video player)
- NVIDIA proprietary drivers will be automatically installed if an NVIDIA card is detected. **Only installed on Glibc systems**
- nano (I prefer it to vim, not sorry)
- OBS Studio (streaming/recording)
- Skype (sadly I still need it)
- Spicetify (CLI tools for Spotify theming) **Optional**
- Spotify
- Steam
- Wine
- XPDF (PDF viewer)
- ZSH (shell) with custom matching theme based on Agnoster

Feel free to find this section of the script and change the applications to those you want to install, or simply delete, or comment out the relevant section.

### Notes
- There is no login manager installed. If you need one, you'll need to install it yourself.
