# Get server name
mullvad_server=$(ip link | grep "mullvad")
if ip link | grep -q "mullvad"; then
  case $mullvad_server in
    *"atlanta"*)
      echo " Atlanta"
      ;;
    *"dallas"*)
      echo " Dallas"
      ;;
    *"denver"*)
      echo " Denver"
      ;;
    *"london"*)
      echo " London"
      ;;
    *"manchester"*)
      echo " Manchester"
      ;;
    *"melbourne"*)
      echo " Melbourne"
      ;;
    *"miami"*)
      echo " Miami"
      ;;
    *"montreal"*)
      echo " Montreal"
      ;;
    *"ny"*)
      echo " New York"
      ;;
    *"seattle"*)
      echo " Seattle"
      ;;
    *"sydney"*)
      echo " Sydney"
      ;;
    *"tokyo"*)
      echo " Tokyo"
      ;;
    *"vancouver"*)
      echo " Vancouver"
      ;;
    *"zurich"*)
      echo " Zurich"
      ;;
  esac
else
  echo "Wireguard Offline"
fi

