# Show pool health (yes/no. Default: "no")
show_health="yes"

# Change pool_name to the name of the pool you want to monitor
pool_name="tank"

# Get pool information. Do not edit
# Pool allocation
pool_alloc=$(zpool list -o alloc $pool_name | tr -d 'ALLOC\n ')
# Total pool size
pool_size=$(zpool list -o size $pool_name | tr -d 'SIZE\n ')
# Pool health status
pool_health=$(zpool list -o health $pool_name)
# End do not edit

# Output pool info
case $show_health in
  *"no"*)
    echo " $pool_alloc|$pool_size"
    ;;
  *"yes"*)
    case $pool_health in
      *"ONLINE"*)
        echo " $pool_alloc|$pool_size (Online)"
        ;;
      *"DEGRADED"*)
        echo " $pool_alloc|$pool_size (Degraded)"
        ;;
      *"FAULTED"*)
        echo " $pool_alloc|$pool_size (Faulted)"
        ;;
      *"OFFLINE"*)
        echo " $pool_alloc|$pool_size (Offline)"
        ;;
      *"UNAVAIL"*)
        echo " $pool_alloc|$pool_size (Unavailable)"
        ;;
      *"REMOVED"*)
        echo " $pool_alloc|$pool_size (Removed)"
        ;;
    esac
esac
